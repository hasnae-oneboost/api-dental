<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CabinetRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
/**
 * @ORM\Entity(repositoryClass=CabinetRepository::class)
 * @ApiResource(normalizationContext={"groups"={"cabinet:read"}},
 *     denormalizationContext={"groups"={"cabinet:write"}})
 * @UniqueEntity(
 * fields={"email"},
 * message="il existe déjà ce email '{{ value }}',veuillez saisir un autre email")
*@UniqueEntity( fields={"nom"},
 * message="il existe dejà ce nom '{{ value }}',veuillez saisir un autre nom")
 * @ApiFilter(BooleanFilter::class, properties={"deleted"})
 */
class Cabinet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("cabinet:read")
     * 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"cabinet:read", "cabinet:write"})
     *  
    
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     * @Groups({"cabinet:read", "cabinet:write"})
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"cabinet:read", "cabinet:write"})
  * @Assert\Regex(
 *     pattern="/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
 *     message="invalide email"
 * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"cabinet:read", "cabinet:write"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"cabinet:read", "cabinet:write"})
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="cabinet")
     * @Groups("cabinet:read")
     */
    private $users;

    /**
     * @ORM\Column(type="boolean", name="deleted")
     *  @Groups({"cabinet:read", "cabinet:write"})
     */
    private $deleted;

    /**
     * @ORM\OneToMany(targetEntity=Patient::class, mappedBy="cabinet")
     * @Groups("cabinet:read")
     */
    private $patients;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->patients = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCabinet($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCabinet() === $this) {
                $user->setCabinet(null);
            }
        }

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @return Collection|Patient[]
     */
    public function getPatients(): Collection
    {
        return $this->patients;
    }

    public function addPatient(Patient $patient): self
    {
        if (!$this->patients->contains($patient)) {
            $this->patients[] = $patient;
            $patient->setCabinet($this);
        }

        return $this;
    }

    public function removePatient(Patient $patient): self
    {
        if ($this->patients->removeElement($patient)) {
            // set the owning side to null (unless already changed)
            if ($patient->getCabinet() === $this) {
                $patient->setCabinet(null);
            }
        }

        return $this;
    }
}
